# Jamf Auth

Jamf Auth provides automatically-refreshing token authentication for the Jamf Pro API to the Python Requests module.

## Install it

To install Jamf Auth, run the following:

```sh
pip install jamf-api-auth
```

Or, place the following in your Pipfile:

```toml
[packages]
jamf-api-auth
```

## Use it

```python
import requests
from jamf_auth import JamfAuth
from os import environ

username = environ["JAMF_USERNAME"]
password = environ["JAMF_PASSWORD"]

with JamfAuth("https://jss.example.com:8443", username, password) as jamfauth:
    r = requests.get("https://jss.example.com:8443/api/v2/mobile-devices", auth=jamfauth)
    print(r.json()["totalCount"])

>>> 42
```

Using JamfAuth in a `with` statement ensures that a token is created and refreshed only for as long as you need. Once Python leaves the `with` statement (even if there is an exception inside), JamfAuth invalidates its token.

Retrieving the token for the first time is slower than refreshing it later, so make sure you're not entering and leaving many `with` statements.

### Advanced usage

#### Improving performance with a Session

If your process requires several requests to the same Jamf server, you can see significantly improved performance using a [requests.Session() object](https://requests.readthedocs.io/en/master/user/advanced/). To make things even easier, you can set your JamfAuth instance as the default auth parameter on your session. Then you won't have to pass it in every time:

```python
with requests.Session() as s, JamfAuth(url, username, password) as jamfauth:
    jamf_session.auth = jamfauth
    r = s.get("https://jss.example.com:8443/api/v2/mobile-devices") # auth= is gone!
    print(r.json()["totalCount"])

>>> 42
```

#### Handling token refresh

Your JamfAuth instance handles refreshing its token automatically. It will check whether it needs to refresh on every use by default. If the check finds that less than 20% of the token's lifespan remains, it will be refreshed with the relatively quick `/auth/keep-alive` endpoint. If `/auth/keep-alive` fails (for example, because the token has already expired), JamfAuth will use the username and password instead.

If you want to make sure your token is valid all the time (for example, you don't want to incur the overhead of logging in with a username and password), run `JamfAuth.refresh_auth_if_needed()` frequently. How often your tokens will expire depends on the configuration of your Jamf server, so run it as often as it makes sense for your environment.

## Develop it

To get started developing Jamf Auth, clone this repository:

```
git clone https://gitlab.com/cvtc/appleatcvtc/jamfauth.git
cd jamfauth
```

This repository contains a Pipfile for easy management of virtual environments
with Pipenv. Run it, but don't create a lockfile, to install the development
dependencies:

```
pipenv install --skip-lock --dev
```

To run the tests and get coverage information, run:

```
pipenv run pytest --cov=jamf_auth --cov-branch --cov-report=xml --cov-report=term
```

We format files with Black prior to committing. Black is installed in your
Pipenv virtual environment. Run it like this before you commit:

```
pipenv run black .
```

## Author

Dalton Durst [@UniversalSuperBox](https://github.com/UniversalSuperBox)